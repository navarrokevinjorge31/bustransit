package dare.poc.transit;

import android.app.Dialog;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private GoogleMap mMap;
    LatLng p = null;
    Double d_longitude , d_latitude , latitude , longitude;
    protected GoogleApiClient mGoogleApiClient;
    protected LocationRequest locationRequest;
    GPSTracker gps;
    private static final LatLng PH = new LatLng(14.5654904,121.083279);
    Dialog infoDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        checkingGPS();
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        gps = new GPSTracker(MapsActivity.this);
        if (gps.canGetLocation()) {
            d_latitude = gps.getLatitude();
            d_longitude = gps.getLongitude();
        } else {
            gps.showSettingsAlert();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        MapStyleOptions style;
        style = MapStyleOptions.loadRawResourceStyle(this, R.raw.mapstyle_retro);
        p = new LatLng(d_latitude,d_longitude);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(p, 5));
        mMap.setMapStyle(style);
        p = new LatLng(14.5644747,121.0867767);
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(
                new CameraPosition.Builder()
                        .target(p)
                        .tilt(60)
                        .zoom(15)
                        .bearing(0)
                        .build()));
        ArrayList<LatLng> coordList = new ArrayList<LatLng>();
        coordList.add(new LatLng(14.559722, 121.083639));
        coordList.add(new LatLng(14.560662, 121.081010));
        coordList.add(new LatLng(14.559140, 121.080511));
        coordList.add(new LatLng(14.558171, 121.083039));
        coordList.add(new LatLng(14.559718, 121.083649));
        coordList.add(new LatLng(14.562114, 121.084548));
        coordList.add(new LatLng(14.563838, 121.084993));
        coordList.add(new LatLng(14.564572, 121.085100));
        coordList.add(new LatLng(14.565661, 121.085629));
        coordList.add(new LatLng(14.568425, 121.085965));
        coordList.add(new LatLng(14.571932, 121.087904));
        coordList.add(new LatLng(14.571018, 121.091579));
        coordList.add(new LatLng(14.571148, 121.093894));
        coordList.add(new LatLng(14.571369, 121.097260));
        coordList.add(new LatLng(14.571107, 121.098614));
        coordList.add(new LatLng(14.572940, 121.100292));
        coordList.add(new LatLng(14.575577, 121.098560));
        coordList.add(new LatLng(14.575150, 121.093812));
        coordList.add(new LatLng(14.575431, 121.089585));
        coordList.add(new LatLng(14.571908, 121.087871));
        PolylineOptions polylineOptions = new PolylineOptions();
        polylineOptions.addAll(coordList);
        polylineOptions
                .width(15)
                .color(R.color.colorAccent);
        mMap.addPolyline(polylineOptions);
        mMap.addMarker(new MarkerOptions().title("GAD Office").position(new LatLng(14.560131, 121.081999)).icon(BitmapDescriptorFactory.fromResource(R.drawable.bus_station)));
        mMap.addMarker(new MarkerOptions().title("San Miguel Elementary School").position(new LatLng(14.565562, 121.085746)).icon(BitmapDescriptorFactory.fromResource(R.drawable.bus_station)));
        mMap.addMarker(new MarkerOptions().title("Villa Eusebio").position(new LatLng(14.571943, 121.096243)).icon(BitmapDescriptorFactory.fromResource(R.drawable.bus_station)));
        mMap.addMarker(new MarkerOptions().title("Rave Park").position(new LatLng(14.575144, 121.097335)).icon(BitmapDescriptorFactory.fromResource(R.drawable.bus_station)));
        mMap.addMarker(new MarkerOptions().title("20 mins away").position(new LatLng( 14.565255, 121.085460)).icon(BitmapDescriptorFactory.fromResource(R.drawable.bus_marker)));
        mMap.addMarker(new MarkerOptions().title("30 mins away").position(new LatLng( 14.575197, 121.092732)).icon(BitmapDescriptorFactory.fromResource(R.drawable.bus_marker)));
        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                showInfoDialog();
            }
        });
    }

    private void checkingGPS(){
        gps = new GPSTracker(MapsActivity.this);
        if (gps.canGetLocation()){
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
            Log.d("longlat",String.valueOf(latitude)+"\n"+String.valueOf(longitude));
            //latitudes = String.valueOf(latitude);
            //longitudes = String.valueOf(longitude);

        } else {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            mGoogleApiClient.connect();
            locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);

            locationRequest.setFastestInterval(5 * 1000);
        }
    }

    public void showInfoDialog(){
        infoDialog = new Dialog(this);
        infoDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        infoDialog.setContentView(R.layout.xml_info_dialog);
        Window window = infoDialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawableResource(R.drawable.rounded_dialog);
        Button dialog_close = infoDialog.findViewById(R.id.dialog_close);
        dialog_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                infoDialog.cancel();
            }
        });
        infoDialog.show();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }
}
