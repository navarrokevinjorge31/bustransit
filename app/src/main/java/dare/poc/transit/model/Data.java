package dare.poc.transit.model;

public class Data {

    private String latitude;
    private String longitude;
    private String mode;

    public Data(String latitude, String longitude, String mode, String mode_info, String route) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.mode = mode;
        this.mode_info = mode_info;
        this.route = route;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getMode_info() {
        return mode_info;
    }

    public void setMode_info(String mode_info) {
        this.mode_info = mode_info;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    private String mode_info;
    private String route;


}
