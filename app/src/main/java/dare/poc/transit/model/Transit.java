package dare.poc.transit.model;

import com.google.gson.annotations.SerializedName;

public class Transit {
    /**
     * id : 41
     * route : 1
     * mode : bus
     * mode_info : bus_1_route_1
     * lat : 12.2132100
     * long : 12.3232300
     * created_at : 2019-04-12 05:19:39
     * updated_at : 2019-04-12 05:19:39
     */
    @SerializedName("id")
    private int id;
    @SerializedName("route")
    private String route;
    @SerializedName("mode")
    private String mode;
    @SerializedName("mode_info")
    private String mode_info;
    @SerializedName("lat")
    private String lat;
    @SerializedName("long")
    private String longX;
    @SerializedName("created_at")
    private String created_at;
    @SerializedName("updated_at")
    private String updated_at;

    public Transit(int id, String route, String mode, String mode_info, String lat, String longX, String created_at, String updated_at) {
        this.id = id;
        this.route = route;
        this.mode = mode;
        this.mode_info = mode_info;
        this.lat = lat;
        this.longX = longX;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getMode_info() {
        return mode_info;
    }

    public void setMode_info(String mode_info) {
        this.mode_info = mode_info;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLongX() {
        return longX;
    }

    public void setLongX(String longX) {
        this.longX = longX;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
