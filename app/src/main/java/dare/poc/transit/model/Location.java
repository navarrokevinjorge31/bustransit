package dare.poc.transit.model;

import com.google.gson.annotations.SerializedName;

public class Location {

    @SerializedName("id")
    public Integer id;
    @SerializedName("routeId")
    public String routeId;
    @SerializedName("longitude")
    public String longitude;
    @SerializedName("latitude")
    public String latitude;
    @SerializedName("created_at")
    public String created_at;
    @SerializedName("updated_at")
    public String updated_at;

    public Location(Integer id, String routeId, String longitude, String latitude, String created_at, String updated_at) {
        this.id = id;
        this.routeId = routeId;
        this.longitude = longitude;
        this.latitude = latitude;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public String getLongitude() {
            return longitude;
        }

    public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

    public String getLatitude() {
            return latitude;
        }

    public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

    public String getCreated_at() {
            return created_at;
        }

    public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

    public String getUpdated_at() {
            return updated_at;
        }

    public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

}
