package dare.poc.transit.bus;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import dare.poc.transit.GPSTracker;
import dare.poc.transit.R;
import dare.poc.transit.remote.RetrofitClient;
import dare.poc.transit.response.BusResponse;
import retrofit2.Call;
import retrofit2.Callback;

public class GPSSender extends Activity {

    Double longitude , latitude , speed;
    public static final String TAG = GPSSender.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xml_gps_sender);


    }



    public void modeTracking(){
        Call<BusResponse> call = RetrofitClient
                .getInstance(getApplicationContext()).getApi().sendData1("test", String.valueOf(longitude),String.valueOf(latitude));

        call.enqueue(new Callback<BusResponse>() {
            @Override
            public void onResponse(Call<BusResponse> call, retrofit2.Response<BusResponse> response) {
                Log.e("response",response.body().getMessage());
            }

            @Override
            public void onFailure(Call<BusResponse> call, Throwable t) {
                Log.e(TAG , "Something went wrong!" + t.getMessage());

            }
        });

    }
}