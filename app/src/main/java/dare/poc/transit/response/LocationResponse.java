package dare.poc.transit.response;

import com.google.gson.annotations.SerializedName;

import dare.poc.transit.model.Location;

public class LocationResponse {

    @SerializedName("bus_locations")
    private Location bus_locationsX;

    public Location getBus_locationsX() {
        return bus_locationsX;
    }

    public void setBus_locationsX(Location bus_locationsX) {
        this.bus_locationsX = bus_locationsX;
    }
}
