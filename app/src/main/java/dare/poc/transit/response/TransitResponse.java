package dare.poc.transit.response;

import com.google.gson.annotations.SerializedName;

import dare.poc.transit.model.Transit;

public class TransitResponse {

    @SerializedName("latlong")
    private Transit latlong;

    public Transit getLatlong() {
        return latlong;
    }

    public void setLatlong(Transit latlong) {
        this.latlong = latlong;
    }

}
