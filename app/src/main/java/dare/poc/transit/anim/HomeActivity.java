package dare.poc.transit.anim;

import android.Manifest;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.SquareCap;

import java.util.ArrayList;
import java.util.List;
import dare.poc.transit.R;
import dare.poc.transit.bus.GPSSender;
import dare.poc.transit.location.FeedLocation;
import dare.poc.transit.remote.RetrofitClient;
import dare.poc.transit.response.LocationResponse;
import retrofit2.Call;
import retrofit2.Callback;

import static com.google.android.gms.maps.model.JointType.ROUND;
import static dare.poc.transit.anim.MapUtils.getBearing;

public class HomeActivity extends AppCompatActivity implements OnMapReadyCallback {

    private static final long DELAY = 1500;
    private static final long ANIMATION_TIME_PER_ROUTE = 1500;
    String polyLine = "_x}wAg{bbV?k@?i@A}A|GgE`F{ClDrCjE|Dw@pFLhD@T@T`@bNJpMqDfVfJhEpIrD`H`@fET|@Fp@JxDjBlCRbIlAhNzDeAhEwB|HrGzAp@EpDiNyHsBkN{DcIkAiCUyDkBq@K_AGgESaHc@qIqDeJkEnDkVKmMaMFaJLi@yIUiFSsD";
    GoogleMap googleMap;
    private PolylineOptions polylineOptions;
    private Polyline greyPolyLine;
    private SupportMapFragment mapFragment;
    private Handler handler;
    private Marker carMarker , carMarker1;

    private LatLng startPosition;
    private LatLng endPosition;
    private LatLng startPosition1;
    private LatLng endPosition1;

    private float v;
    List<LatLng> polyLineList;
    private double lat, lng;



    double latitude = 14.5906359;
    double longitude = 120.9785071;
//    double latitude = 14.57549;
//    double longitude = 121.09716;
    double latitude1 = 14.56844;
    double longitude1 = 121.08597;

    Button button2;
    private String TAG = "HomeActivity";
    private boolean isFirstPosition = true;
    private boolean isFirstPosition1 = true;
    TextView txt_stopA , txt_stopB , txt_stopC , txt_stopD;
    String id = "1";
    String id1 = "40";
    int speedMetersPerMinute = 60;

    private GeofencingClient geofencingClient;
    private static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        checkPermissions();

        geofencingClient = LocationServices.getGeofencingClient(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("BUS TRANSIT");
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_launcher);
        loadBottomSheet();

        //Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        button2 = findViewById(R.id.button2);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        txt_stopA = findViewById(R.id.txt_stopA);
        txt_stopB = findViewById(R.id.txt_stopB);
        txt_stopC = findViewById(R.id.txt_stopC);
        txt_stopD = findViewById(R.id.txt_stopD);
        handler = new Handler();
    }

    String[] permissions = new String[]{
            Manifest.permission.INTERNET,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION};

    private boolean checkPermissions() {
        int result;

        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(HomeActivity.this, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    void loadBottomSheet(){

        // get the bottom sheet view
        LinearLayout llBottomSheet = findViewById(R.id.bottom_sheet);
        // init the bottom sheet behavior
        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(llBottomSheet);
        // change the state of the bottom sheet
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_DRAGGING);
        // set the peek height
        bottomSheetBehavior.setPeekHeight(340);
        // set hideable or not
        bottomSheetBehavior.setHideable(false);
        // set callback for changes
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
            }
            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });
    }

    void staticPolyLine() {

        googleMap.clear();
        googleMap.addMarker(new MarkerOptions().title("GAD Office").position(new LatLng(14.56020,121.08224)).icon(BitmapDescriptorFactory.fromResource(R.drawable.bus_station)));
        googleMap.addMarker(new MarkerOptions().title("San Miguel Elementary School").position(new LatLng(14.565698, 121.085638)).icon(BitmapDescriptorFactory.fromResource(R.drawable.bus_station)));
        googleMap.addMarker(new MarkerOptions().title("Villa Eusebio").position(new LatLng(14.57128,121.09657)).icon(BitmapDescriptorFactory.fromResource(R.drawable.bus_station)));
        googleMap.addMarker(new MarkerOptions().title("Rave Park").position(new LatLng(14.57549,121.09716)).icon(BitmapDescriptorFactory.fromResource(R.drawable.bus_station)));
        polyLineList = MapUtils.decodePoly(polyLine);
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (LatLng latLng : polyLineList) {
            builder.include(latLng);
        }
        LatLngBounds bounds = builder.build();
        CameraUpdate mCameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, 2);
        googleMap.animateCamera(mCameraUpdate);

        polylineOptions = new PolylineOptions();
        polylineOptions.color(Color.BLACK);
        polylineOptions.width(8);
        polylineOptions.startCap(new SquareCap());
        polylineOptions.endCap(new SquareCap());
        polylineOptions.jointType(ROUND);
        polylineOptions.addAll(polyLineList);
        greyPolyLine = googleMap.addPolyline(polylineOptions);
        Log.e("PolylineList",String.valueOf(polyLineList));
        //drawCar(latitude, longitude);
        //drawCar1(latitude1, longitude1);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.setMapType(R.raw.mapstyle_retro);
        googleMap.setTrafficEnabled(false);
        googleMap.setIndoorEnabled(false);
        googleMap.setBuildingsEnabled(false);
        googleMap.getUiSettings().setZoomControlsEnabled(false);
    }

    private void getLive_Locations(){

        Call<LocationResponse> call = RetrofitClient
                .getInstance(getApplicationContext()).getApi().getMode_Locations1(id);
        call.enqueue(new Callback<LocationResponse>() {

            @Override
            public void onResponse(Call<LocationResponse> call, retrofit2.Response<LocationResponse> response) {

//                String longlat = response.body().getBus_locationsX().longitude + "," + response.body().getBus_locationsX().latitude;
//                int idx = response.body().getBus_locationsX().id;
//                if(idx == 96){
//                    id = "1";
//                }else{
//                    id = String.valueOf(idx+1);
//                }
//                Log.e("Locations: ",id + " : " + longlat);

                Double startLatitude1 = Double.valueOf(response.body().getBus_locationsX().latitude);
                Double startLongitude1 = Double.valueOf(response.body().getBus_locationsX().longitude);

                if (isFirstPosition) {
                    startPosition = new LatLng(latitude, longitude);
                    isFirstPosition = false;
                }else {
                    endPosition = new LatLng(startLatitude1, startLongitude1);
                    Log.d(TAG, startPosition.latitude + "--" + endPosition.latitude + "--Check --" + startPosition.longitude + "--" + endPosition.longitude);
                    if ((startPosition.latitude != endPosition.latitude) || (startPosition.longitude != endPosition.longitude)) {
                        Log.e(TAG, "NOT SAME");
                        startCarAnimation(startPosition, endPosition);
                    } else {
                        Log.e(TAG, "SAME");
                    }
                }
            }

            @Override
            public void onFailure(Call<LocationResponse> call, Throwable t) {
                Log.e("Locations: ",t.getMessage());
            }
        });
    }

    private void getLive_Locations1(){

        Call<LocationResponse> call = RetrofitClient
                .getInstance(getApplicationContext()).getApi().getMode_Locations1(id1);
        call.enqueue(new Callback<LocationResponse>() {

            @Override
            public void onResponse(Call<LocationResponse> call, retrofit2.Response<LocationResponse> response) {

                String longlat = response.body().getBus_locationsX().longitude + "," + response.body().getBus_locationsX().latitude;
                int idx = response.body().getBus_locationsX().id;
                if(idx == 96){
                    id1 = "1";
                }else{
                    id1 = String.valueOf(idx+1);
                }
                Log.e("Locations1: ",id1 + " : " + longlat);

                Double startLatitude1 = Double.valueOf(response.body().getBus_locationsX().longitude);
                Double startLongitude1 = Double.valueOf(response.body().getBus_locationsX().latitude);

                if (isFirstPosition1) {
                    startPosition1 = new LatLng(latitude1, longitude1);
                    isFirstPosition1 = false;
                }else {
                    endPosition1 = new LatLng(startLatitude1, startLongitude1);
                    Log.d(TAG, startPosition1.latitude + "--" + endPosition1.latitude + "--Check --" + startPosition1.longitude + "--" + endPosition1.longitude);
                    if ((startPosition1.latitude != endPosition1.latitude) || (startPosition1.longitude != endPosition1.longitude)) {
                        Log.e(TAG, "NOT SAME");
                        startCarAnimation1(startPosition1, endPosition1);
                    } else {
                        Log.e(TAG, "SAME");
                    }
                }
            }

            @Override
            public void onFailure(Call<LocationResponse> call, Throwable t) {
                Log.e("Locations: ",t.getMessage());
            }
        });
    }

    private void startCarAnimation(final LatLng start, final LatLng end) {
        Log.i(TAG, "startCarAnimation called...");
        ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
        valueAnimator.setDuration(ANIMATION_TIME_PER_ROUTE);
        valueAnimator.setInterpolator(new LinearInterpolator());
        valueAnimator.addUpdateListener( new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                startPosition = new LatLng(end.latitude,end.longitude);
                v =  valueAnimator.getAnimatedFraction();
                lng = v * end.longitude + (1 - v)
                        * start.longitude;
                lat = v * end.latitude + (1 - v)
                        * start.latitude;
                LatLng newPos = new LatLng(lat, lng);
                busStopA(end.latitude,end.longitude);
                carMarker.setPosition(newPos);
                carMarker.setAnchor(0.5f, 0.5f);
                carMarker.setRotation(getBearing(start, end));
            }
        });
        valueAnimator.start();
    }

    private void busStopA(Double latitude , Double longitude){
        Location stopA = new Location("");
        stopA.setLatitude(14.560131);
        stopA.setLongitude(121.081999);

        Location busA = new Location("");
        busA.setLatitude(latitude);
        busA.setLongitude(longitude);

        float distanceInMetersA = busA.distanceTo(stopA);
        float estimatedDriveTimeInMinutesA = distanceInMetersA / speedMetersPerMinute;
        txt_stopA.setText("GAD OFFICE \n Arrival Time: " + String.format("%.0f", estimatedDriveTimeInMinutesA)+"mins");
    }

    private void startCarAnimation1(final LatLng start, final LatLng end) {
        Log.i(TAG, "startCarAnimation called...");
        ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
        valueAnimator.setDuration(ANIMATION_TIME_PER_ROUTE);
        valueAnimator.setInterpolator(new LinearInterpolator());
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                startPosition1 = new LatLng(end.latitude,end.longitude);
                v = valueAnimator.getAnimatedFraction();
                lng = v * end.longitude + (1 - v)
                        * start.longitude;
                lat = v * end.latitude + (1 - v)
                        * start.latitude;
                LatLng newPos = new LatLng(lat, lng);
                carMarker1.setPosition(newPos);
                carMarker1.setAnchor(0.5f, 0.5f);
                carMarker1.setRotation(getBearing(start, end));
            }
        });
        valueAnimator.start();
    }

    private void drawCar(Double latitude, Double longitude) {
        LatLng latLng = new LatLng(latitude, longitude);
        carMarker = googleMap.addMarker(new MarkerOptions().position(latLng).flat(true).icon(BitmapDescriptorFactory.fromResource(R.mipmap.new_bus1)));
        handler.postDelayed(staticCarRunnable, 500);
    }

    private void drawCar1(Double latitude, Double longitude) {
        LatLng latLng = new LatLng(latitude, longitude);
        carMarker1 = googleMap.addMarker(new MarkerOptions().position(latLng).flat(true).icon(BitmapDescriptorFactory.fromResource(R.mipmap.new_bus)));
    }

    Runnable staticCarRunnable = new Runnable() {
        @Override
        public void run() {
            getLive_Locations();
            //getLive_Locations1();
            handler.postDelayed(this, 10000);
        }
    };

    void stopRepeatingTask() {
        if (staticCarRunnable != null) {
            handler.removeCallbacks(staticCarRunnable);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopRepeatingTask();
    }

    Runnable mStatusChecker =  new Runnable() {
        @Override
        public void run() {
            try {
                //getDriverLocationUpdate();
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
            handler.postDelayed(mStatusChecker, DELAY);
        }
    };

    void startGettingOnlineDataFromCar() {
        handler.post(mStatusChecker);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //staticPolyLine();
                googleMap.clear();
                drawCar(latitude, longitude);
                startGettingOnlineDataFromCar();
                break;

            case R.id.action_settings:
                Intent intent = new Intent(HomeActivity.this, FeedLocation.class);
                startActivity(intent);
                break;
        }

        return super.onOptionsItemSelected(item);
    }


}
