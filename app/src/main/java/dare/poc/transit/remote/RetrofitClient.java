package dare.poc.transit.remote;

import android.content.Context;
import java.io.IOException;
import dare.poc.transit.BuildConfig;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    //
    private static final String BASE_URL = BuildConfig.DEFAULT_BASE_URL;
    private static RetrofitClient mInstance;
    private Retrofit retrofit;
    protected Context context;


    public RetrofitClient(final Context context) {
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(
                        new Interceptor() {
                            @Override
                            public Response intercept(Chain chain) throws IOException {
                                Request original = chain.request();
                                //authorization check
                                //if(SharedPreferenceManager.getInstance(context).haveToken() ) {
                                //    Request.Builder requestBuilder = original.newBuilder()
                                //            .addHeader("Authorization", "Bearer " + SharedPreferenceManager.getInstance(context).getToken())
                                //            .method(original.method(), original.body());
                                //    Request    request = requestBuilder.build();
                                //    return chain.proceed(request);
                                //}else {
                                //    Request.Builder requestBuilder = original.newBuilder()
                                //            .method(original.method(), original.body());
                                //    Request request = requestBuilder.build();
                                //    return chain.proceed(request);
                                //}
                                Request.Builder requestBuilder = original.newBuilder()
                                        .method(original.method(), original.body());
                                Request request = requestBuilder.build();
                                return chain.proceed(request);
                            }
                        }
                ).build();

                retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
    }

    public static synchronized RetrofitClient getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new RetrofitClient(context);
        }
        return mInstance;
    }

    public ApiInterface getApi() {
        return retrofit.create(ApiInterface.class);
    }

}

