package dare.poc.transit.remote;

import dare.poc.transit.response.BusResponse;
import dare.poc.transit.response.LocationResponse;
import dare.poc.transit.response.TransitResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("latlong")
    Call<BusResponse> sendData(
            @Field("lat") String latitude,
            @Field("long") String longitude,
            @Field("mode") String mode,
            @Field("mode_info") String mode_info,
            @Field("route") String route);

    @FormUrlEncoded
    @POST("bus_locations")
    Call<BusResponse> sendData1(
            @Field("routeId") String routeId,
            @Field("longitude") String longitude,
            @Field("latitude") String latitude);


    @GET("get_bus_locations/{id}")
    Call<LocationResponse> getMode_Locations(@Path("id") String id);

    @GET("get_bus_locations/{id}")
    Call<LocationResponse> getMode_Locations1(@Path("id") String id);

    @GET("get_latlong")
    Call<TransitResponse> getLat_Long();

}
